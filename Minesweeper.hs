import System.IO
import Data.Maybe

data Action = 	NoOp
		| Flag
		| Uncover
		deriving (Eq, Show, Read)

data UState = 	Untouched
		| Uncovered
		| Flagged
		deriving (Eq, Show)

data BState =	Bomb
		| NoBomb
		deriving (Eq, Show)

type Field = (BState, UState)

type Board = [[Field]]

data Position = Position Int Int
		deriving (Eq, Show)

data Command = 	Command Action Position
		deriving (Eq, Show)

at :: Board -> Position -> Field
at board (Position x y) = board !! y !! x

isOutOfBounds :: [a] -> Position -> Bool
isOutOfBounds list pos@(Position x y) = x < 0 || y < 0 || x >= len || y >= len
	where
		len = length list

safeAt :: Board -> Position -> Field
safeAt board pos@(Position x y)
	| isOutOfBounds board pos	= (NoBomb, Untouched)
	| otherwise			= at board pos
	where
		len = length board

bombAt :: Board -> Position -> Bool
bombAt board pos = case safeAt board pos of
	(Bomb, _) 	-> True
	_		-> False

countAdjacent :: Board -> Position -> String
countAdjacent board pos = case at board pos of
	(NoBomb, Uncovered) 	-> showCount
	(Bomb, Uncovered)	-> showCount
	(_, Flagged)		-> "F"
	_			-> " "
	where
		showCount = show $ countAdjacent' board pos

countAdjacent' :: Board -> Position -> Int
countAdjacent' board (Position x y) = sum $ map (\ipos -> if bombAt board ipos then 1 else 0)
	[Position (x - 1) (y - 1)
	,Position (x - 1) y
	,Position (x - 1) (y + 1)
	,Position x (y - 1)
	,Position x (y + 1)
	,Position (x + 1) (y - 1)
	,Position (x + 1) y
	,Position (x + 1) (y + 1)
	]

convertAdjacent :: Board -> [[String]]
convertAdjacent board = [[countAdjacent board (Position x y) | x <- gen] | y <- gen]
	where
		gen = take (length board) [0..]

replaceAt' :: [a] -> Int -> a -> [a]
replaceAt' input key replace = begin ++ (replace : ending)
	where
		begin = take key input
		ending = drop (key + 1) input

replaceAt :: [[a]] -> Position -> a -> [[a]]
replaceAt input (Position x y) replace = replaceAt' input y $ replaceAt' (input !! y) x replace

initializeBoard :: Int -> Board
initializeBoard size = 	[[(Bomb, Untouched), (Bomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched)]
			,[(NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched)]
			,[(NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched)]
			,[(NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched)]
			,[(NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched), (NoBomb, Untouched)]
			]
--take size $ repeat $ take size $ repeat $ (NoBomb, Untouched)

readAction :: String -> Action
readAction "flag" 	= Flag
readAction "uncover" 	= Uncover
readAction _		= NoOp

analyzeInput :: String -> Maybe Command
analyzeInput input
	| length sentence == 3	= do
			let (command:x:y:[]) = sentence
			return $ Command (readAction command) (Position (read x) (read y))
	| otherwise		= Nothing
	where
		sentence = words input

uncover :: Board -> Position -> Board
uncover board pos = replaceAt board pos (NoBomb, Uncovered)

recurseUncover :: Board -> Position -> Board
recurseUncover board pos
	| countAdjacent' newBoard pos == 0 	= fst $ recurseUncover' (newBoard, [pos]) pos
	| otherwise 				= newBoard
	where
		newBoard = uncover board pos


recurseUncover' :: (Board, [Position]) -> Position -> (Board, [Position])
recurseUncover' inp (Position x y) = foldl helper inp
	[Position (x + 1) y
	,Position (x - 1) y
	,Position x (y + 1)
	,Position x (y - 1)
	]
	where
		helper :: (Board, [Position]) -> Position -> (Board, [Position])
		helper input@(oldBoard, track) pos = if isOutOfBounds oldBoard pos || elem pos track || countAdjacent' oldBoard pos > 0
			then	input
			else	case at newBoard pos of
					(NoBomb, _)	-> if countAdjacent' oldBoard pos == 0 then recurseUncover' (newBoard, pos:track) pos else input
					_		-> input
			where
				newBoard = uncover oldBoard pos
			
	

executeCommand :: Board -> Command -> Maybe Board
executeCommand board (Command action pos) = do
	case action of
		Flag	-> case field of
				(bonb, Flagged) 	-> return $ replaceMin (bonb, Untouched)
				(bonb, Untouched) 	-> return $ replaceMin (bonb, Flagged)
				_			-> return board
		Uncover -> case field of
				(Bomb, _)		-> Nothing
				(NoBomb, _)		-> return $ recurseUncover board pos
		NoOp	-> return board
	where	field = at board pos
		replaceMin = replaceAt board pos


doMagic :: Board -> String -> Maybe Board
doMagic board input =
	case command of
		Nothing 	-> return board
		Just com	-> do
			newBoard <- executeCommand board com
			return newBoard
	where command = analyzeInput input

loop :: Board -> IO ()
loop initialBoard = do
	putStrLn $ unlines $ map concat $convertAdjacent initialBoard
	putStrLn "Enter a command, please :-)"
	input <- getLine
	case doMagic initialBoard input of
		Just newBoard	-> loop newBoard
		Nothing		-> putStrLn "Guess I'm done."

main :: IO ()
main = do
	putStrLn "Initializing..."
	putStr "Enter board size: "
	hFlush stdout
	input <- getLine
	let board = initializeBoard $ (read input::Int)
	loop board
